
#import "TIMViewController.h"
#import "TIMNumberClass.h"

@implementation TIMViewController

TIMNumberClass* player=nil;
+ (void) initialize{
    if(!player){
        player = [[TIMNumberClass alloc] init];
    }
}

- (IBAction)whenUserDidTouchYellow:(id)sender {
    [self buttonNo];
    [self switchOnYellow];
    [self compareColors:@"yellow"];
}
- (IBAction)whenUserDidTouchGreen:(id)sender {
    [self buttonNo];
    [self switchOnGreen];
    [self compareColors:@"green"];
}
- (IBAction)whenUserDidTouchBlue:(id)sender {
    [self buttonNo];
    [self switchOnBlue];
    [self compareColors:@"blue"];
}
- (IBAction)whenUserDidTouchRed:(id)sender {
    [self buttonNo];
    [self switchOnRed];
    [self compareColors:@"red"];
}

- (IBAction)userDidTouchPegulation:(id)sender {
    [self alertView:1];
}

- (IBAction)touchEndOfGame:(id)sender {
    alertViewOnOf=1;
    [self endOfGame];
}

- (void) recordLabel: (NSInteger) rec{
    self.record.text =[NSString stringWithFormat:@"%u",(unsigned int)rec];
}

- (IBAction)playGame:(id)sender {
    [self buttonPlayAndRegYesOrNo:0];
    [self buttonNo];
    timer=[player levelUpp:[arrayRandomColor count]];
    arrayRandomColor = [player randObjectAddArray:arrayRandomColor];
    factor=2;
    numColor=0;
    [self swithColor];
}

-(void) swithColor{
    for (int i=0; i<[arrayRandomColor count]; i++) {
        NSString * color=arrayRandomColor[i];
        if ([color isEqual:@"yellow"]){
            [self performSelector:@selector(switchOnYellow) withObject:nil afterDelay:timer*i*factor];
            }
        else{
            if([color isEqual:@"green"]){
               [self performSelector:@selector(switchOnGreen) withObject:nil afterDelay:timer*i*factor];
            }
            else{
                if([color isEqual:@"blue"]){
                   [self performSelector:@selector(switchOnBlue) withObject:nil afterDelay:timer*i*factor];
                }
                else{
                    if([color isEqual:@"red"]){
                       [self performSelector:@selector(switchOnRed) withObject:nil afterDelay:timer*i*factor];
                    }}}}
    }
    [self buttonYes];
}

-(void)compareColors:(NSString*)color {
    if(numColor<[arrayRandomColor count]){
        if([color isEqual: arrayRandomColor[numColor]]){
            numColor++;
            [self buttonYes];
            if(numColor==[arrayRandomColor count]){
                [self performSelector:@selector(playGame:) withObject:nil afterDelay:timer*factor*1.5];
                self.point.text=[NSString stringWithFormat:@"%u",(unsigned int)[arrayRandomColor count]];
                NSInteger rec=[player recordUser: numColor];
                [self recordLabel: rec];
                [self buttonNo];
            }
        }
        else {
            [self endOfGame];
        }
    }
}

-(void) endOfGame{
    [self switchOffColor];
    [self buttonNo];
    [arrayRandomColor removeAllObjects];
    timer=0;
    factor=0;
    numColor=0;
    self.point.text=[NSString stringWithFormat:@"%d",0];
    if(!alertViewOnOf) [self alertView:0];
    alertViewOnOf=0;
    [self buttonPlayAndRegYesOrNo:1];
}

-(void) alertView: (BOOL) alertView{
    if(alertView){
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Rules!" message:@"The game lights up tiles with different colors in some order. You should repeat that by touching tiles in the same sequence.\n Game ends when mistake is made." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
    if(!alertView){
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"You lose!" message:@"If you want start all over again push the button: 'OK' and 'Play!'" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }

}

-(void) switchOnYellow{
    self.whenUserDidTouchYellow.alpha=1;
    [self performSelector:@selector(switchOffColor) withObject:nil afterDelay:timer];
}
-(void) switchOnGreen{
    self.whenUserDidTouchGreen.alpha=1;
    [self performSelector:@selector(switchOffColor) withObject:nil afterDelay:timer];
}
-(void) switchOnBlue{
    self.whenUserDidTouchBlue.alpha=1;
    [self performSelector:@selector(switchOffColor) withObject:nil afterDelay:timer];
}
-(void) switchOnRed{
    self.whenUserDidTouchRed.alpha=1;
    [self performSelector:@selector(switchOffColor) withObject:nil afterDelay:timer];
}

-(void) switchOffColor{
    self.whenUserDidTouchYellow.alpha=0.5;
    self.whenUserDidTouchGreen.alpha=0.5;
    self.whenUserDidTouchBlue.alpha=0.5;
    self.whenUserDidTouchRed.alpha=0.5;
}

-(void) buttonPlayAndRegYesOrNo:(BOOL)buttonOnOff{
    if (!buttonOnOff){
        self.playGame.enabled=NO;
        self.regulationTouch.enabled=NO;
        self.playGame.alpha=0.2;
        self.regulationTouch.alpha=0.2;
        self.propTouchEndOfGame.enabled=YES;
        self.propTouchEndOfGame.alpha=1;
    }
    if (buttonOnOff){
        self.playGame.enabled=YES;
        self.regulationTouch.enabled=YES;
        self.playGame.alpha=1;
        self.regulationTouch.alpha=1;
        self.propTouchEndOfGame.enabled=NO;
        self.propTouchEndOfGame.alpha=0.2;
    }
}

-(void)buttonYes{
    self.whenUserDidTouchYellow.enabled=YES;
    self.whenUserDidTouchGreen.enabled=YES;
    self.whenUserDidTouchBlue.enabled=YES;
    self.whenUserDidTouchRed.enabled=YES;
}
-(void)buttonNo{
    self.whenUserDidTouchYellow.enabled=NO;
    self.whenUserDidTouchGreen.enabled=NO;
    self.whenUserDidTouchBlue.enabled=NO;
    self.whenUserDidTouchRed.enabled=NO;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [self switchOffColor];
    arrayRandomColor=[[NSMutableArray alloc] init];
    [self buttonNo];
    self.propTouchEndOfGame.enabled=NO;
    self.propTouchEndOfGame.alpha=0.2;
    NSInteger rec=[player recordUser: numColor];
    [self recordLabel: rec];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}

@end
