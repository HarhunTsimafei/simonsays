//
//  TIMNumberClass.m
//  simonSays
//
//  Created by Timofei Harhun on 09.10.14.
//  Copyright (c) 2014 tima. All rights reserved.
//

#import "TIMNumberClass.h"

@implementation TIMNumberClass

-(NSMutableArray*) randObjectAddArray:(NSMutableArray*) array {
    NSArray *color=[[NSArray alloc] initWithObjects:@"yellow",@"green",@"blue",@"red",nil];
    NSInteger randVariable = arc4random()%4;
    [array addObject:color[randVariable]];
    return array;
}

-(CGFloat) levelUpp: (NSInteger)sizeOfArray{
    CGFloat time=0.f;
    if(sizeOfArray<=5)time=0.5f;
    if(sizeOfArray>5 && sizeOfArray<=8) time=0.4f;
    if(sizeOfArray>8 && sizeOfArray<=13) time=0.3f;
    if(sizeOfArray>13) time=0.2f;
    
    return time;
}

- (NSInteger) recordUser: (NSInteger) rec{
    NSUserDefaults* variable = [[NSUserDefaults alloc]init];
    NSInteger recordView=0;
    NSInteger a=[variable integerForKey:@"record"];
    recordView=a;
    if(a<rec){
        [variable setInteger:rec forKey:@"record"];
        recordView=rec;
    }
    return recordView;
}





@end
