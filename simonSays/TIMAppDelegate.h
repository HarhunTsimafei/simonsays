//
//  TIMAppDelegate.h
//  simonSays
//
//  Created by Timofei Harhun on 17.09.14.
//  Copyright (c) 2014 tima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TIMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
