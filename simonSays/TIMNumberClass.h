//
//  TIMNumberClass.h
//  simonSays
//
//  Created by Timofei Harhun on 09.10.14.
//  Copyright (c) 2014 tima. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TIMNumberClass : NSObject

-(NSMutableArray*)randObjectAddArray:(NSMutableArray*) array;
-(CGFloat) levelUpp: (NSInteger)sizeOfArray;
- (NSInteger) recordUser: (NSInteger) rec;

@end
