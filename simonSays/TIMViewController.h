
#import <UIKit/UIKit.h>

@interface TIMViewController : UIViewController{
    NSMutableArray *  arrayRandomColor;
    CGFloat timer;
    NSInteger factor, numColor;
    bool alertViewOnOf;
}

@property (weak, nonatomic) IBOutlet UIButton *whenUserDidTouchYellow;
@property (weak, nonatomic) IBOutlet UIButton *whenUserDidTouchGreen;
@property (weak, nonatomic) IBOutlet UIButton *whenUserDidTouchBlue;
@property (weak, nonatomic) IBOutlet UIButton *whenUserDidTouchRed;
@property (weak, nonatomic) IBOutlet UIButton *playGame;
@property (weak, nonatomic) IBOutlet UILabel *point;
@property (weak, nonatomic) IBOutlet UIButton *regulationTouch;
@property (weak, nonatomic) IBOutlet UIButton *propTouchEndOfGame;
@property (weak, nonatomic) IBOutlet UILabel *record;


- (IBAction)userDidTouchPegulation:(id)sender;
- (IBAction)touchEndOfGame:(id)sender;

-(void) switchOnYellow;
-(void) switchOnGreen;
-(void) switchOnBlue;
-(void) switchOnRed;
-(void) switchOffColor;

-(void) buttonYes;
-(void) buttonNo;

@end
